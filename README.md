# README #

Implement a virtual Ping-Pong championship, which features 8 players and 1 referee. The 8 players will be instances of the same class, with different attributes as defined in the table below. All players and referee should be separate & independent threads. The Referee starts and waits all 8 players to join the championship. When all players have joined, the referee draws the 4 initial games and notifies the players about their game id, opponent and their order of play (first, second). 

All games are knock-out and supervised by the referee. After all 4 games have ended, the referee informs the defeated players to shut down, draws the second round (semi finals), informs the players about their new game id and opponents. In a similar fashion, the process continues to the final game and cup winner. 

The game starts with the first (offensive) player picking one random number (from 1 to 10) and informing the referee about it. The defending player creates a defence array of random numbers (from 1 to 10). The length of the defence array is preset for each player (see players matrix at the end of this document) and defined in their individual configuration. If the number picked by the offensive player does not exist in the defence array, then the player gets one point and plays again. If it exists, the defender gets the point and they switch roles (defender attacks). The first player to get 5 points wins the game.

![Screen Shot 2015-10-12 at 4.03.53 pm.png](https://bitbucket.org/repo/RRKebd/images/1603772758-Screen%20Shot%202015-10-12%20at%204.03.53%20pm.png)
package championship;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

import controllers.PlayerController;
import controllers.RefereeController;
import models.Player;

public class Championship {

	private static final int[] defenceSetLengths = new int[] {8, 7, 6, 8, 5, 6, 6, 8};
	private static final String[] playerNames = new String[] {"Joey", "Nick", "Russel", "Vivek", 
		"Pritam", "Amit", "Chandler", "Calwin"};
	
	public static ConcurrentHashMap<String, PlayerController> playersMap;
	public static CopyOnWriteArrayList<PlayerController> playersList;
	static ThreadGroup playersGroup;
	static CountDownLatch startSignal;
	static CyclicBarrier gameSetSignal;
	static CountDownLatch playersJoined;
	
	public static void main(String[] args) {
		
		startSignal = new CountDownLatch(1);
		playersJoined = new CountDownLatch(8);
		
		RefereeController refController = new RefereeController()
												.setStartSignal(startSignal)
												.setPlayersJoined(playersJoined);
		
		refController.start();
		
		playersGroup = new ThreadGroup("Players Group");
		playersList = new CopyOnWriteArrayList<PlayerController>();
		for(int i=0; i<defenceSetLengths.length; i++){
			PlayerController playerController = new PlayerController(playersGroup, 
														new Player(playerNames[i], defenceSetLengths[i]))
													.setStartSignal(startSignal)
													.setPlayersJoined(playersJoined);
			playersList.add(playerController);
			playerController.start();
		}
		
		try {
			Thread.sleep(1000*10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		while(playersGroup.activeCount() > 1){
			
		}
		
	}
	
}

package controllers;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

import championship.Championship;
import models.Referee;

public class RefereeController extends Thread {

	CountDownLatch startSignal;
	CountDownLatch setComplete;
	CyclicBarrier gameSetSignal;
	CountDownLatch playersJoined;
	ThreadGroup playersGroup;
	Referee model;
	ConcurrentHashMap<String, PlayerController> playersMap;
	static CopyOnWriteArrayList<PlayerController> playersList;
	ConcurrentHashMap<Integer, PlayerController[]> gameMap;
	Runnable gameSetRunnable;
	int completedGames;
	
	public RefereeController() {
		this.model = new Referee();
	}
	
	
	@Override
	public void run() {
		super.run();
		
		try {
			System.out.println("Waiting for all the players to join...");
			playersJoined.await();
			System.out.println("All the players have joined, so now referee needs to draw games.");
			
			gameSetRunnable = new Runnable() {
				
				@Override
				public void run() {
					checkRound();
				}
			};
			
			playersMap = Championship.playersMap;
			playersList = Championship.playersList;
			
			while(playersList.size() > 1){
				
				setComplete = new CountDownLatch(1);
				drawGames();
				completedGames = 0;
				startSignal.countDown();
				setComplete.await();
				keepWinners();
			}
			Thread.sleep(100);
			PlayerController controller = playersList.get(0);
			System.out.println("Winner is: "+controller.getName());
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

	public void keepWinners(){
		for(PlayerController player : playersList){
			if(!player.model.isWonGame()){
				player.model.setShutDown(true);
				playersList.remove(player);
			}
		}
	}
	
	public void checkRound(){
		Iterator<Entry<Integer, PlayerController[]>> it = gameMap.entrySet().iterator();
		startSignal = new CountDownLatch(1);
		while(it.hasNext()){
			Map.Entry<Integer, PlayerController[]> pair = it.next();
			PlayerController player1 = pair.getValue()[0];
			PlayerController player2 = pair.getValue()[1];
			player1.startSignal = startSignal;
			player2.startSignal = startSignal;
			
			if(player1.getModel().isPlayingGame() && player2.getModel().isPlayingGame()){
				
				if(player2.getModel().isDefender()){
					int attackNumber = player1.getModel().getOffensiveNumber();
					int[] defenceSet = player2.getModel().getDefenceSet();
					if(wasAttackSuccessful(attackNumber, defenceSet)){
						player1.model.IncrementPoint();
					} else {
						player1.model.setDefender(true);
						player2.model.setDefender(false);
						player2.model.IncrementPoint();
					}
				} else {
					int attackNumber = player2.getModel().getOffensiveNumber();
					int[] defenceSet = player1.getModel().getDefenceSet();
					if(wasAttackSuccessful(attackNumber, defenceSet)){
						player2.model.IncrementPoint();
					} else {
						player1.model.setDefender(false);
						player2.model.setDefender(true);
						player1.model.IncrementPoint();
					}
				}
				
				if(player1.model.getPoints() == 5 || player2.model.getPoints() == 5){
					completedGames++;
					player1.model.setPlayingGame(false);
					player2.model.setPlayingGame(false);
				}
				
			}
		}
		
		if(completedGames == gameMap.size()){
			setComplete.countDown();
		} else {
			startSignal.countDown();
		}
		
		printScore();
		
	}
	
	public static boolean wasAttackSuccessful(int attackNumber, int[] defenceSet){
		
		for(int i=0; i<defenceSet.length; i++){
			if(attackNumber == defenceSet[i])
				return false;
		}
		
		return true;
	}
	
	public void drawGames(){
		gameMap = new ConcurrentHashMap<Integer, PlayerController[]>();
		int number_of_players = playersList.size();
		if((number_of_players & (number_of_players-1)) != 0) {
			IllegalArgumentException e = new IllegalArgumentException("Knock Out Tournament "
					+ "needs number of players in the power of 2.");
			throw(e);
		}
		
		gameSetSignal = new CyclicBarrier(number_of_players, gameSetRunnable);
		int gameId = 0;
		for(int i=0; i<playersList.size(); i+=2){
			PlayerController player1 = playersList.get(i);
			PlayerController player2 = playersList.get(i+1);
			player1.setNewGameData(gameId, player2.getName(), false);
			player1.setGameSetSignal(gameSetSignal);
			player2.setNewGameData(gameId, player1.getName(), true);
			player2.setGameSetSignal(gameSetSignal);
			gameMap.put(gameId, new PlayerController[]{player1, player2});
			System.out.println("########################################################");
			System.out.println("GameId: "+gameId);
			System.out.println("Player1: "+player1.model.toString());
			System.out.println("Player2: "+player2.model.toString());
			gameId++;
		}
		System.out.println("########################################################");
		
	}
	
	public CountDownLatch getStartSignal() {
		return startSignal;
	}

	public RefereeController setStartSignal(CountDownLatch startSignal) {
		this.startSignal = startSignal;
		return this;
	}

	public CyclicBarrier getGameSetSignal() {
		return gameSetSignal;
	}

	public RefereeController setGameSetSignal(CyclicBarrier gameSetSignal) {
		this.gameSetSignal = gameSetSignal;
		return this;
	}

	public CountDownLatch getPlayersJoined() {
		return playersJoined;
	}

	public RefereeController setPlayersJoined(CountDownLatch playersJoined) {
		this.playersJoined = playersJoined;
		return this;
	}

	public ThreadGroup getPlayersGroup() {
		return playersGroup;
	}

	public RefereeController setPlayersGroup(ThreadGroup playersGroup) {
		this.playersGroup = playersGroup;
		return this;
	}
	
	public void printGameMap(){
		Iterator<Entry<Integer, PlayerController[]>> it = gameMap.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<Integer, PlayerController[]> pair = it.next();
			int gameId  = pair.getKey();
			PlayerController player1 = pair.getValue()[0];
			PlayerController player2 = pair.getValue()[1];
			System.out.println("########################################################");
			System.out.println("GameId: "+gameId);
			System.out.println("Player1: "+player1.model.toString());
			System.out.println("Player2: "+player2.model.toString());
		}
		System.out.println("########################################################");
	}
	
	public void printScore(){
		Iterator<Entry<Integer, PlayerController[]>> it = gameMap.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<Integer, PlayerController[]> pair = it.next();
			int gameId  = pair.getKey();
			PlayerController player1 = pair.getValue()[0];
			PlayerController player2 = pair.getValue()[1];
			System.out.println("########################################################");
			System.out.println("GameId: "+gameId);
			System.out.println("Player1: "+player1.model.getName()+" Points: "+player1.model.getPoints());
			System.out.println("Player2: "+player2.model.getName()+" Points: "+player2.model.getPoints());
		}
		System.out.println("########################################################");
	}
}

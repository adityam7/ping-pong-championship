package controllers;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

import models.Player;

public class PlayerController extends Thread {

	CountDownLatch startSignal;
	CyclicBarrier gameSetSignal;
	CountDownLatch playersJoined;
	Player model;
	
	public PlayerController(ThreadGroup threadGroup, Player model) {
		super(threadGroup, model.getName());
		this.model = model;
	}
	
	@Override
	public void run() {
		super.run();
		
		playersJoined.countDown();
		System.out.println(this.getName()+" joined.");
		
		try {
			while(!model.isShutDown()){
				
				//System.out.println(getName()+" waiting for Start Signal");
				startSignal.await();
				//System.out.println(getName()+" got the Start Signal");
				
				if(model.isPlayingGame()){
					if(model.isDefender()){
						model.setUpDefenceSet();
					} else {
						model.GenerateOffenceNumber();
					}
				}
					
				try {
					gameSetSignal.await();
				} catch (BrokenBarrierException e) {
					e.printStackTrace();
				}
				
			}
			
		} catch (InterruptedException e) {
			e.printStackTrace();
			return;
		}
		
	}

	public void setNewGameData(int gameID, String opponentName, boolean defender){
		model.setGameId(gameID)
			.setOpponentName(opponentName)
			.setDefender(defender)
			.setPoints(0)
			.setPlayingGame(true)
			.setWonGame(false);
	}
	
	public CountDownLatch getStartSignal() {
		return startSignal;
	}

	public PlayerController setStartSignal(CountDownLatch startSignal) {
		this.startSignal = startSignal;
		return this;
	}

	public CyclicBarrier getGameSetSignal() {
		return gameSetSignal;
	}

	public PlayerController setGameSetSignal(CyclicBarrier gameSetSignal) {
		this.gameSetSignal = gameSetSignal;
		return this;
	}

	public CountDownLatch getPlayersJoined() {
		return playersJoined;
	}

	public PlayerController setPlayersJoined(CountDownLatch playersJoined) {
		this.playersJoined = playersJoined;
		return this;
	}

	public Player getModel() {
		return model;
	}

	public void setModel(Player model) {
		this.model = model;
	}
	
}

package models;

import java.util.Arrays;
import java.util.Random;

public class Player {

	int gameId;
	String opponentName;
	boolean defender;
	int offensiveNumber;
	int defenceSetLength;
	int[] defenceSet;
	String name;
	int points;
	boolean wonGame;
	boolean playingGame;
	boolean shutDown;
	
	public Player (String name, int defenceSetLenght){
		this.name = name;
		this.defenceSetLength = defenceSetLenght;
		defenceSet = new int[this.defenceSetLength];
		setUpDefenceSet();
	}
	
	public int GenerateOffenceNumber(){
		Random randomNumberGenerator = new Random();
		offensiveNumber = randomNumberGenerator.nextInt(9)+1;
		return offensiveNumber;
	}
	
	public boolean SuccessfulDefenceAgainst(int offensiveNumber){
		for(int i=0; i<defenceSetLength; i++){
			if(defenceSet[i] == offensiveNumber){
				return true;
			}
		}
		return false;
	}
	
	public void setUpDefenceSet(){
		Random randomNumberGenerator = new Random();
		for(int i=0; i<defenceSetLength; i++){
			defenceSet[i] = randomNumberGenerator.nextInt(9)+1;
		}
	}
	
	public boolean isPlayingGame() {
		return playingGame;
	}

	public Player setPlayingGame(boolean playingGame) {
		this.playingGame = playingGame;
		return this;
	}

	public int getGameId() {
		return gameId;
	}

	public Player setGameId(int gameId){
		this.gameId = gameId;
		return this;
	}

	public String getOpponentName() {
		return opponentName;
	}

	public Player setOpponentName(String opponentName) {
		this.opponentName = opponentName;
		return this;
	}

	public boolean isDefender() {
		return defender;
	}

	public Player setDefender(boolean defender) {
		this.defender = defender;
		return this;
	}

	public int getOffensiveNumber() {
		return offensiveNumber;
	}

	public void setOffensiveNumber(int offensiveNumber) {
		this.offensiveNumber = offensiveNumber;
	}

	public int getDefenceSetLength() {
		return defenceSetLength;
	}

	public int[] getDefenceSet() {
		return defenceSet;
	}

	public String getName() {
		return name;
	}

	public int getPoints() {
		return points;
	}

	public Player setPoints(int points) {
		this.points = points;
		isWonGame();
		return this;
	}

	public void IncrementPoint(){
		this.points++;
		isWonGame();
	}
	
	public boolean isWonGame() {
		if(points < 5)
			wonGame = false;
		else
			wonGame = true;
		return wonGame;
	}

	public Player setWonGame(boolean wonGame) {
		this.wonGame = wonGame;
		return this;
	}

	public boolean isShutDown() {
		return shutDown;
	}

	public void setShutDown(boolean shutDown) {
		this.shutDown = shutDown;
	}

	@Override
	public String toString() {
		return "Player [ name="
				+ name + ", points=" + points + ", gameId=" + gameId + ", opponentName=" + opponentName
				+ ", defender=" + defender + ", offensiveNumber="
				+ offensiveNumber + ", defenceSetLength=" + defenceSetLength
				+ ", defenceSet=" + Arrays.toString(defenceSet) + ", wonGame=" + wonGame
				+ ", playingGame=" + playingGame + "]";
	}

	
}

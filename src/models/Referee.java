package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class Referee {

	HashMap<Integer, Player[]> games;
	
	public void DrawGames(ArrayList<Player> players) throws IllegalArgumentException{
		if(players.size()%2 != 0){
			IllegalArgumentException e = new IllegalArgumentException("Number "
					+ "of players must be even for a knockout tournament");
			throw e;
		}
		games = new HashMap<Integer, Player[]>();
		int gameId = 0;
		for(int i=0; i<players.size(); i=i+2){
			players.get(i).setGameId(gameId)
				.setOpponentName(players.get(i+1).getName())
				.setDefender(false)
				.setPoints(0)
				.setPlayingGame(true)
				.setWonGame(false);
			players.get(i+1).setGameId(gameId)
				.setOpponentName(players.get(i).getName())
				.setDefender(true)
				.setPoints(0)
				.setPlayingGame(true)
				.setWonGame(false);
			Player[] couple = {players.get(i), players.get(i+1)};
			games.put(gameId, couple);
			gameId++;
		}
	}
	
	public ArrayList<Player> PlayRound(){
		Iterator<Entry<Integer, Player[]>> it = games.entrySet().iterator();
		ArrayList<Player> winners = new ArrayList<Player>();
		while(it.hasNext()){
			Map.Entry<Integer, Player[]> pair = it.next();
			Player player1 = pair.getValue()[0];
			Player player2 = pair.getValue()[1];
			while(player1.getPoints() < 5 && player2.getPoints() < 5){
				Player offence, defence;
				
				if(player1.isDefender()){
					offence = player2;
					defence = player1;
				}else{
					offence = player1;
					defence = player2;
				}
				
				defence.setUpDefenceSet();
				if(defence.SuccessfulDefenceAgainst(
						offence.GenerateOffenceNumber())){
					defence.IncrementPoint();
					defence.setDefender(false);
					offence.setDefender(true);
				} else {
					offence.IncrementPoint();
				}
				
			}
			if(player1.isWonGame()){
				winners.add(player1);
				player2.setShutDown(true);
			} else {
				winners.add(player2);
				player1.setShutDown(true);
			}
		}
		return winners;
	}
}
